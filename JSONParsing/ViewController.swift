//
//  ViewController.swift
//  JSONParsing
//
//  Created by Martin Burolla on 3/24/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController {

    // MARK: - Data members
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
    }

    // MARK: - JSON Processing

    private func getData() {
        let urlString = "https://api.whitehouse.gov/v1/petitions.json?limit=1"
            
        if let url = NSURL(string: urlString) {
            if let data = try? NSData(contentsOfURL: url, options: []) {
                let json = JSON(data: data)
                parseData(json)
            }
        }
    }
    
    private func parseData(json: JSON) {
        guard json["metadata"]["responseInfo"]["status"].intValue == HTTPStatusCode.Success.rawValue else { return }
        
        for result in json["results"].arrayValue {
            let p = Petition()
            p.title = result["title"].stringValue
            p.body = result["body"].stringValue
            p.signature = result["signatureCount"].stringValue
            print(p)
        }
    }
}
