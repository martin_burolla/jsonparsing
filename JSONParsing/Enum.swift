//
//  Enum.swift
//  JSONParsing
//
//  Created by Martin Burolla on 3/24/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import Foundation

enum HTTPStatusCode: Int {
    case Success = 200
    case NotFound = 400
    case ServerError = 500
}

