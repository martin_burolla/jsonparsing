//
//  Petition.swift
//  JSONParsing
//
//  Created by Martin Burolla on 3/24/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import Foundation

class Petition : CustomStringConvertible {
    var title = ""
    var body = ""
    var signature = ""
    
    var description: String { return "\(title)\(body)\(signature)" }
}