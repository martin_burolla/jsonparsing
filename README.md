# JSONParsing
Helper project in Swift that shows how to parse JSON

### Description

Uses SwiftyJSON to parse data from https://api.whitehouse.gov/.

### API Calls

https://api.whitehouse.gov/v1/petitions.json?limit=1

<img src=https://raw.githubusercontent.com/mburolla/ReadMePix/master/HelperRepos/hr.png width=1024 height=100>